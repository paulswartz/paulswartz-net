<?php
/* Copyright © 2022 Jamie Zawinski <jwz@jwz.org>

   Permission to use, copy, modify, distribute, and sell this software and its
   documentation for any purpose is hereby granted without fee, provided that
   the above copyright notice appear in all copies and that both that
   copyright notice and this permission notice appear in supporting
   documentation.  No representations are made about the suitability of this
   software for any purpose.  It is provided "as is" without express or
   implied warranty.

   Allows you to be discoverable on Mastodon with your own domain name,
   without needing to run your own server.  Makes "you@your-domain" be
   an alias for "also-you@the-actual-mastodon-server".

   Created: 8-Nov-2022.

   Installation:

    - Edit $ACCOUNTS, below.
    - Install this script in "/.well-known/webfinger"
    - Make it be runnable, e.g. by putting this in "/.well-known/.htaccess" --
      <Files "webfinger">
        ForceType application/x-httpd-php
      </Files>
 */

$ACCOUNTS = [
  //
  // Alias account name		Real underlying account name
  //
  'paul@paulswartz.net' => 'paulswartz@urbanists.social',
  'm@paulswartz.net' => 'paulswartz@urbanists.social',
  'f@paulswartz.net' => 'paulswartz@odd.town'
  ];

function webfinger() {
  global $ACCOUNTS;

  $acct = $_GET['resource'] ?? null;
  if (! $acct) error ("unparsable");

  if (! preg_match ('/^(?:acct:)?@?(.*)$/s', $acct, $m))
    error ("unparsable acct: $acct");
  $acct = $m[1];

  $redir = $ACCOUNTS[strtolower($acct)] ?? null;
  if (!$redir) error ("no match: $acct");

  if (! preg_match ('/@(.*)$/s', $redir, $m))
    error ("internal error: no domain");
  $domain = $m[1];

  $url = "https://$domain/.well-known/webfinger?resource=acct:$redir";

  header ("HTTP/1.1 301 Moved Permanently");
  header ("Location: $url");
  header ("Content-Type: text/plain");
}

function error($err) {
  error_log ($err);
  header ("HTTP/1.1 400 $err");
  header ("Content-Type: text/plain");
  print "$err\n";
  exit (1);
}

webfinger();
exit (0);
